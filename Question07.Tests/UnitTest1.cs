using Question07.Console;

namespace Question07.Tests;

public class UnitTest1
{
    [Fact]
    public async Task GoodSequence()
    {
        var result = @"Station received message: pong
Ship receive: ping
Station received temperature: 200
Station received message: ping
Ship receive: pong
Station received temperature: 200
Station received message: pang
Ship receive: repeat
Station received temperature: 200
Station received message: pong
Ship receive: ping
Station received temperature: 200
Station received message: pong
Ship receive: ping
Station received temperature: 200
Station received message: end
";

        var stringWriter = new StringWriter();
        StationCommunicatorOrchestrator test = new StationCommunicatorOrchestrator(stringWriter);
        await test.ManageCommunication();
        Assert.Equal(result, stringWriter.ToString());
    }

}
