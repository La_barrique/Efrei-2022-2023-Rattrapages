﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question07.Console
{
    public class SpatialCommunication
    {
        private readonly TextWriter writer;
        private IReadOnlyList<string> messages = new List<string>()
        {
            "pong",
            "ping",
            "pang",
            "pong",
            "pong",
            "end"
        };
        private int readMessageIndex = 0;
        ManualResetEventSlim temperatureAsked = new ManualResetEventSlim();
        ManualResetEventSlim messageSended = new ManualResetEventSlim(true);
        ManualResetEventSlim messageReceived = new ManualResetEventSlim();

        public SpatialCommunication(TextWriter writer)
        {
            this.writer = writer;

        }

        public Task<string> ReceiveMessage()
        {
            messageReceived.Set();
            messageSended.Wait();
            messageSended.Reset();
            
            string message;
            if (readMessageIndex >= messages.Count)
                message = "";
            else
                message = messages[readMessageIndex++];
            return Task.FromResult(message);
        }

        public Task SendMessage(string message)
        {
            messageSended.Set();
            temperatureAsked.Wait();
            temperatureAsked.Reset();
            
            writer.WriteLine("Ship receive: " + message);
            return Task.CompletedTask;
        }

        public Task<int> AskTemperature()
        {
            temperatureAsked.Set();
            messageReceived.Wait();
            messageReceived.Reset();
            return Task.FromResult(200);
        }
    }
}
